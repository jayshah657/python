import webbrowser
import time

total_break = 3 # How many breaks would you like?
break_count = 0

print("This program started on: " + time.ctime()) # In your terminal, it will print the current time of this program was excuted.

while(break_count < total_break):
    time.sleep(7200) # 7200 = 2 hours and it will count for 7200 secs when take_break is ran
    webbrowser.open("https://www.google.com") # Put your unique link here. It will open the given link when program counts 2 hours
    break_count += 1 # It will repeat until it reachs the given total_break

""" This program is just for fun. I am learning new concepts in python :)"""

