# let's understand how list [] and tuples () play roles in python

# list lets you edit your data
numbers = [1,2,3,4,4,4,5,6,7,8,9]
print(numbers) # will print all the numbers
for n in numbers:
    if n > 4:
        print(n) #this will print all the numbers above 4
numbers.append(3) #adding 3 to the list
print(numbers.sort()) # this will sort the numbers
print(numbers.count(4)) #It will count how many 4's are there

# lets run the list

#uncomment the below code and comment the above code, if needed

#tuples "You can't change anything in tuples data"

# fav_num = (11, 2, 3, 4, 5) #fav_nums dont change right?
# print(fav_num)