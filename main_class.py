#Let's understand why developer use if __name__ == '__main__'(create a module so they can use it for different script)

def name(n):
    print("Hello there " + n)
name('Jay')

if __name__ == '__main__': # you can now use this as import modules
    print('Well, It printed this but not the name function!')

# look for main_class_2 for output, when i have used this file as module (import)