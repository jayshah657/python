#classes and instances
class friends():
    num_total_friends = 0
    def __init__(self, firstName, lastName, age, hobbies):
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
        self.hobbies = hobbies

        # I would like to add all the friends database count to num_total_friends
        friends.num_total_friends += 1

friends_1 = friends('Aman', 'Kumar', 25, 'Businesses')
friends_2 = friends('Soham', 'Patel', 24, 'Drinking')
friends_3 = friends('moe', 'Patel', 27, 'Drinking')

print(friends.num_total_friends) # should print out 2


